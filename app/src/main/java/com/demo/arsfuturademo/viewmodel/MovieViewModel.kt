package com.demo.arsfuturademo.viewmodel

import androidx.lifecycle.MutableLiveData
import com.demo.data.repository.MovieRepository
import com.demo.domain.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(private val movieRepository: MovieRepository): BaseViewModel() {

    val getFavoriteMoviesSuccess = MutableLiveData<List<Movie>>()
    val searchMoviesSuccess = MutableLiveData<List<Movie>>()
    val getMovieSuccess = MutableLiveData<Movie>()

    fun getFavoriteMovies() {
        executeUseCase {
            val favorites = movieRepository.getFavoriteMovies()
            getFavoriteMoviesSuccess.postValue(favorites)
        }
    }

    fun searchMovies(keyword: String?) {
        keyword?.let {
            executeUseCase {
                val result = movieRepository.searchMovies(keyword)?.sortedBy { it.year }
                result?.let { searchMoviesSuccess.postValue(it) }
            }
        }
    }

    fun getMovie(movieId: String?) {
        movieId?.let {
            executeUseCase {
                val movie = movieRepository.getMovie(it)
                getMovieSuccess.postValue(movie)
            }
        }
    }

    fun toggleFavoriteMovie(movie: Movie) {
        executeUseCase {
            when (movie.isFavorite) {
                true -> movieRepository.removeFromFavorites(movie)
                false -> movieRepository.addToFavorites(movie)
                else -> {}
            }
        }
    }
}