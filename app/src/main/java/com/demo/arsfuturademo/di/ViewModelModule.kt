package com.demo.arsfuturademo.di

import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.data.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class ViewModelModule {

    @Provides
    fun provideMovieViewModel(movieRepository: MovieRepository) = MovieViewModel(movieRepository)
}