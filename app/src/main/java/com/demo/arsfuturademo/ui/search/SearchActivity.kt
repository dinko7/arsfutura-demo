package com.demo.arsfuturademo.ui.search

import android.os.Bundle
import android.widget.SearchView
import androidx.activity.viewModels
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.databinding.SearchActivityBinding
import com.demo.arsfuturademo.ui.base.BaseActivity
import com.demo.arsfuturademo.ui.common.VerticalSpacingItemDecoration
import com.demo.arsfuturademo.ui.common.stickyheader.StickyHeaderDecoration
import com.demo.arsfuturademo.ui.movie.MovieActivity
import com.demo.arsfuturademo.ui.utils.DialogUtil
import com.demo.arsfuturademo.ui.utils.Utils
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : BaseActivity<SearchActivityBinding>(R.layout.search_activity) {
    private val searchMoviesAdapter = SearchMoviesRecyclerViewAdapter()
    private val viewModel: MovieViewModel by viewModels()
    private var searchQuery: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupUI()
        observeViewModel()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onStart() {
        searchQuery?.let {
            binding.searchBar.clearFocus()
            viewModel.searchMovies(it)
        }
        super.onStart()
    }

    private fun setupUI() {
        setupSearchView()
        setupRecyclerView()
    }

    private fun setupSearchView() {
        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchQuery = query
                viewModel.searchMovies(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    private fun setupRecyclerView() {
        binding.recyclerView.adapter = searchMoviesAdapter
        searchMoviesAdapter.setItemClickListener { view, position, movie ->
            if (view.id == R.id.favorite_button)
                toggleFavorite(movie, position)
            else
                startActivity(MovieActivity.buildIntent(this, movie.id))
        }
        binding.recyclerView.apply {
            addItemDecoration(VerticalSpacingItemDecoration(40))
            addItemDecoration(StickyHeaderDecoration(searchMoviesAdapter, false, false))
        }
    }

    private fun toggleFavorite(movie: Movie, position: Int) {
        val toggleFavorite = movie.isFavorite?.not()
        viewModel.toggleFavoriteMovie(movie)
        searchMoviesAdapter.replaceItemAt(movie.apply { isFavorite = toggleFavorite }, position)
    }

    private fun observeViewModel() {
        viewModel.searchMoviesSuccess.observe(this) {
            searchMoviesAdapter.addAll(it)
        }
        viewModel.anyUseCaseFailure.observe(this) {
            Utils.showError(this, it)
        }
        viewModel.anyUseCaseInProgress.observe(this) {
            DialogUtil.toggleProgressDialog(supportFragmentManager, it)
        }
    }
}