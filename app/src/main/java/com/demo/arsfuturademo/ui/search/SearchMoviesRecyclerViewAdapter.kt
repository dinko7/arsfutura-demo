package com.demo.arsfuturademo.ui.search

import android.view.View
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.databinding.SearchMoviesViewHolderBinding
import com.demo.arsfuturademo.databinding.YearStickyHeaderItemDecorationBinding
import com.demo.arsfuturademo.ui.base.BaseStickyHeaderRecyclerViewAdapter
import com.demo.domain.Movie

class SearchMoviesRecyclerViewAdapter:
    BaseStickyHeaderRecyclerViewAdapter<YearStickyHeaderItemDecorationBinding, SearchMoviesViewHolderBinding, Movie>(
        R.layout.year_sticky_header_item_decoration,
        R.layout.search_movies_view_holder
    ) {

    override fun getMyHeaderId(position: Int): Long {
        return (getItem(position).year?.substring(0,4) ?: "0").toLong()
    }

    override fun onBindHeader(headerBinding: YearStickyHeaderItemDecorationBinding, position: Int) {
        getItem(position).year?.let {
            headerBinding.header.text = it
        }
    }

    override fun onBind(
        binding: SearchMoviesViewHolderBinding,
        position: Int,
        item: Movie,
        vararg payloads: MutableList<Any>
    ) {
        binding.movie = item
    }

    override fun setClickableView(binding: SearchMoviesViewHolderBinding): List<View?> = listOf(binding.root, binding.favoriteButton)
}