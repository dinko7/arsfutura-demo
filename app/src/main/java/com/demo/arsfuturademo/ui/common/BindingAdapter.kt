package com.demo.arsfuturademo.ui.common

import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("android:visibleIf")
    fun View.visibleIf(visible: Boolean?) {
        visibility = if (visible == true) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("android:imageUrl")
    fun ImageView.loadImageFromUrl(imageUrl: String?) {
        imageUrl?.let {
            Picasso.get().load(Uri.parse(it)).into(this)
        }
    }
}