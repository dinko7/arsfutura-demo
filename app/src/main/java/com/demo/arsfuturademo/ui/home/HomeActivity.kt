package com.demo.arsfuturademo.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.databinding.HomeActivityBinding
import com.demo.arsfuturademo.ui.base.BaseActivity
import com.demo.arsfuturademo.ui.common.VerticalSpacingItemDecoration
import com.demo.arsfuturademo.ui.movie.MovieActivity
import com.demo.arsfuturademo.ui.search.SearchActivity
import com.demo.arsfuturademo.ui.utils.DialogUtil
import com.demo.arsfuturademo.ui.utils.Utils
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity: BaseActivity<HomeActivityBinding>(R.layout.home_activity) {
    private val favoritesAdapter = FavoriteMoviesRecyclerViewAdapter()
    private val viewModel: MovieViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.activity = this
        setupRecyclerView()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getFavoriteMovies()
    }

    private fun setupRecyclerView() {
        binding.favoritesRecyclerView.adapter = favoritesAdapter
        favoritesAdapter.setItemClickListener { view, position, movie ->
            if (view.id == R.id.favorite_button)
                removeFromFavorites(movie, position)
            else
                startActivity(MovieActivity.buildIntent(this, movie.id))
        }
        binding.favoritesRecyclerView.addItemDecoration(VerticalSpacingItemDecoration(40))
    }

    private fun removeFromFavorites(movie: Movie, position: Int) {
        viewModel.toggleFavoriteMovie(movie)
        favoritesAdapter.removeItemAt(position)
    }

    private fun observeViewModel() {
        viewModel.getFavoriteMoviesSuccess.observe(this) {
            binding.favoritesEmpty = it.isEmpty()
            favoritesAdapter.addAll(it)
        }
        viewModel.anyUseCaseFailure.observe(this) {
            Utils.showError(this, it)
        }
        viewModel.anyUseCaseInProgress.observe(this) {
            DialogUtil.toggleProgressDialog(supportFragmentManager, it)
        }
    }

    fun openSearchActivity() = startActivity(Intent(this, SearchActivity::class.java))
}