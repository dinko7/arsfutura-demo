package com.demo.arsfuturademo.ui.movie

import android.content.Context
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import com.demo.arsfuturademo.R

class MovieGenreView(context: Context): AppCompatTextView(context) {

    init {
        TextViewCompat.setTextAppearance(this, R.style.MovieGenreText)
        setBackgroundResource(R.drawable.background_round_red)
        setPadding(16,6,16,6)
    }
}