package com.demo.arsfuturademo.ui.utils

import android.app.Activity
import com.google.android.material.snackbar.Snackbar

object Utils {

    fun showMessage(activity: Activity, message: String, duration: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(activity.findViewById(android.R.id.content), message, duration).show()
    }

    fun showError(activity: Activity, error: Throwable, duration: Int = Snackbar.LENGTH_SHORT) {
        showMessage(activity, error.localizedMessage ?: error.stackTraceToString(), duration)
    }
}