package com.demo.arsfuturademo.ui.home

import android.view.View
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.databinding.FavoriteMovieViewHolderBinding
import com.demo.arsfuturademo.databinding.SearchMoviesViewHolderBinding
import com.demo.arsfuturademo.ui.base.BaseRecyclerViewAdapter
import com.demo.domain.Movie

class FavoriteMoviesRecyclerViewAdapter: BaseRecyclerViewAdapter<FavoriteMovieViewHolderBinding, Movie>(
    R.layout.favorite_movie_view_holder) {
    override fun onBind(
        binding: FavoriteMovieViewHolderBinding,
        position: Int,
        item: Movie,
        vararg payloads: MutableList<Any>
    ) {
        binding.movie = item
    }

    override fun setClickableView(binding: FavoriteMovieViewHolderBinding): List<View?> = listOf(binding.root, binding.favoriteButton)
}
