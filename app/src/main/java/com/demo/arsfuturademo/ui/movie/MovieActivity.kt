package com.demo.arsfuturademo.ui.movie

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.databinding.MovieActivityBinding
import com.demo.arsfuturademo.ui.base.BaseActivity
import com.demo.arsfuturademo.ui.utils.Utils
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieActivity : BaseActivity<MovieActivityBinding>(R.layout.movie_activity) {

    companion object {
        private const val movieIdKey = "movieId"

        fun buildIntent(context: Context, movieId: String?) =
            Intent(context, MovieActivity::class.java).apply {
                putExtra(movieIdKey, movieId)
            }
    }

    private var appBarMenu: Menu? = null
    private val viewModel: MovieViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        observeViewModel()
        viewModel.getMovie(intent.getStringExtra(movieIdKey))
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_menu, menu)
        appBarMenu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.getMovieSuccess.value?.let {
            when (item.itemId) {
                R.id.is_favorite -> {
                    showOnlyNotFavoriteMenuIcon()
                    viewModel.toggleFavoriteMovie(it)
                }
                R.id.is_not_favorite -> {
                    showOnlyFavoriteMenuIcon()
                    viewModel.toggleFavoriteMovie(it)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateFavoriteMenuIconVisibility(id: Int, isVisible: Boolean) {
        appBarMenu?.findItem(id)?.isVisible = isVisible
    }

    private fun showOnlyFavoriteMenuIcon() {
        updateFavoriteMenuIconVisibility(R.id.is_not_favorite, false)
        updateFavoriteMenuIconVisibility(R.id.is_favorite, true)
    }

    private fun showOnlyNotFavoriteMenuIcon() {
        updateFavoriteMenuIconVisibility(R.id.is_favorite, false)
        updateFavoriteMenuIconVisibility(R.id.is_not_favorite, true)
    }

    private fun observeViewModel() {
        viewModel.getMovieSuccess.observe(this) {
            binding.movie = it
            addMovieGenres(it)
            title = it.title
            if (it.isFavorite == true)
                showOnlyFavoriteMenuIcon()
            else
                showOnlyNotFavoriteMenuIcon()
        }
        viewModel.anyUseCaseFailure.observe(this) {
            Utils.showError(this, it)
        }
    }

    private fun addMovieGenres(movie: Movie) {
        movie.genres?.forEach {
            binding.genreContainer.addView(
                MovieGenreView(this).apply { text = it }
            )
        }
    }
}