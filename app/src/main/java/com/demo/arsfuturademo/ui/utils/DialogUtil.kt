package com.demo.arsfuturademo.ui.utils

import androidx.fragment.app.FragmentManager
import com.demo.arsfuturademo.ui.common.ProgressBarDialogFragment
import timber.log.Timber

object DialogUtil {

    private var progressBarDialogFragment: ProgressBarDialogFragment? = null

    fun toggleProgressDialog(
        fragmentManager: FragmentManager,
        isLoading: Boolean,
        cancelable: Boolean = false
    ) {
        try {
            if (isLoading && !fragmentManager.fragments.contains(progressBarDialogFragment))
                showProgressBarDialogFragment(fragmentManager, cancelable)
            else hideProgressBarDialogFragment()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun showProgressBarDialogFragment(supportFragmentManager: FragmentManager, cancelable: Boolean = false) {
        try {
            if (progressBarDialogFragment == null) progressBarDialogFragment = ProgressBarDialogFragment()
            progressBarDialogFragment?.let {
                it.isCancelable = cancelable
                it.showNow(supportFragmentManager, it::class.java.simpleName)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun hideProgressBarDialogFragment() {
        try {
            progressBarDialogFragment?.dismiss()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}