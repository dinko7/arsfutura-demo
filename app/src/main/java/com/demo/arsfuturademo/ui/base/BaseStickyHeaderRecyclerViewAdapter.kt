package com.demo.arsfuturademo.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.demo.arsfuturademo.ui.common.stickyheader.StickyHeaderAdapter


/**
 * Prerequisite is that list is sorted
 *
 * Here is the logic behind sticky headers
 * For ex:-
 * Sorted List:    ["Andreas", "Anders" , "Anthony", "Bob", "Cheryl"]
 * Headers:        ["A", "A", "A", "B", "C"]
 * HeadersIds:     [0L,  0L,  0L,  1L,  2L]
 * HeaderPosition: [0,              1,   2]
 * */

abstract class BaseStickyHeaderRecyclerViewAdapter<SVB : ViewDataBinding, VB : ViewDataBinding, T>(
    private val headerLayout: Int,
    itemLayout: Int
) : BaseRecyclerViewAdapter<VB, T>(itemLayout),
    StickyHeaderAdapter<BaseStickyHeaderRecyclerViewAdapter.StickyHeaderHolder<SVB>> {

    class StickyHeaderHolder<SVB : ViewDataBinding>(val binding: SVB) :
        RecyclerView.ViewHolder(binding.root)

    override fun getHeaderId(position: Int): Long = getMyHeaderId(position)

    override fun onCreateHeaderViewHolder(parent: ViewGroup): StickyHeaderHolder<SVB> =
        StickyHeaderHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                headerLayout,
                parent,
                false
            )
        )

    override fun onBindHeaderViewHolder(viewHolder: StickyHeaderHolder<SVB>, position: Int) {
        onBindHeader(viewHolder.binding, position)
    }

    abstract fun getMyHeaderId(position: Int): Long

    abstract fun onBindHeader(headerBinding: SVB, position: Int)
}