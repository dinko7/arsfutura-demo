package com.demo.arsfuturademo.viewmodel

import com.demo.data.repository.MovieRepository
import com.demo.domain.Movie
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class MovieViewModelTest: BaseViewModelTest() {
    @Mock
    private lateinit var movieRepository: MovieRepository

    @InjectMocks
    private lateinit var movieViewModel: MovieViewModel

    @Test
    fun shouldGetFavoriteMovies() = runBlocking {
        // given
        val movieOne = Movie().apply {
            id = "1"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            isFavorite = true
        }
        val response = listOf(movieOne,movieTwo)
        whenever(movieRepository.getFavoriteMovies()).thenReturn(response)

        // when
        movieViewModel.getFavoriteMovies()

        // then
        verify(movieRepository, times(1)).getFavoriteMovies()
        assertEquals(movieViewModel.getFavoriteMoviesSuccess.value, response)
    }

    @Test
    fun shouldGetMovieById() = runBlocking {
        // given
        val movie = Movie().apply { id = "id" }
        whenever(movieRepository.getMovie(movie.id!!)).thenReturn(movie)

        // when
        movieViewModel.getMovie(movie.id)

        // then
        verify(movieRepository, times(1)).getMovie(movie.id!!)
        assertEquals(movieViewModel.getMovieSuccess.value, movie)
    }

    @Test
    fun shouldToggleFavoriteIfMovieWasPreviouslyFavorite() = runBlocking {
        // given
        val movie = Movie().apply {
            id = "id"
            isFavorite = true
        }

        // when
        movieViewModel.toggleFavoriteMovie(movie)

        // then
        verify(movieRepository, times(1)).removeFromFavorites(movie)
    }

    @Test
    fun shouldToggleFavoriteIfMovieWasPreviouslyNotFavorite() = runBlocking {
        // given
        val movie = Movie().apply {
            id = "id"
            isFavorite = false
        }

        // when
        movieViewModel.toggleFavoriteMovie(movie)

        // then
        verify(movieRepository, times(1)).addToFavorites(movie)
    }
}