package com.demo.arsfuturademo.common

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.google.android.material.R

object TestUtils {
    fun assertSnackbarMessageShown(message: String) {
        Espresso.onView(ViewMatchers.withId(R.id.snackbar_text))
            .check(ViewAssertions.matches(ViewMatchers.withText(message)))
    }
}