package com.demo.arsfuturademo.ui.base

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
abstract class BaseActivityTest {

    @get:Rule(order = 1)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule(order = 3)
    var rule: MockitoRule = MockitoJUnit.rule()

    private lateinit var activityScenario: ActivityScenario<out Activity>
    protected abstract val activityClass: Class<out Activity>
    protected lateinit var activityInstance: Activity
        private set

    protected val anyUseCaseInProgress = MutableLiveData<Boolean>()
    protected val anyUseCaseFailure = MutableLiveData<Throwable>()

    protected fun launchActivity(intentExtras: Bundle? = null) {
        val targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        val intent = Intent(targetContext, activityClass)
        intentExtras?.let { intent.putExtras(it) }
        activityScenario = ActivityScenario.launch(intent)
        activityScenario.onActivity { activityInstance = it }
    }

    protected fun waitForUiThread(miliseconds: Long = 0) {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        if (miliseconds > 0) Thread.sleep(miliseconds)
    }

    fun runOnUiThread(action: () -> Unit) {
        activityInstance.runOnUiThread(action)
    }

    @Before
    fun setup() {
        Intents.init()
    }

    @After
    fun tearDown() {
        activityScenario.close()
        Intents.release()
    }
}
