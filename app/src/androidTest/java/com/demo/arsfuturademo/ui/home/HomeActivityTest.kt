package com.demo.arsfuturademo.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.matcher.ViewMatchers.*
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.common.TestUtils
import com.demo.arsfuturademo.ui.base.BaseActivityTest
import com.demo.arsfuturademo.ui.movie.MovieActivity
import com.demo.arsfuturademo.ui.search.SearchActivity
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@HiltAndroidTest
class HomeActivityTest : BaseActivityTest() {
    override val activityClass = HomeActivity::class.java

    @Mock
    @BindValue
    lateinit var viewModel: MovieViewModel

    private val getFavoriteMoviesSuccess = MutableLiveData<List<Movie>>()
    private val searchMoviesSuccess = MutableLiveData<List<Movie>>()
    private val getMovieSuccess = MutableLiveData<Movie>()

    @Before
    fun setUp() {
        whenever(viewModel.anyUseCaseFailure).thenReturn(anyUseCaseFailure)
        whenever(viewModel.anyUseCaseInProgress).thenReturn(anyUseCaseInProgress)
        whenever(viewModel.getFavoriteMoviesSuccess).thenReturn(getFavoriteMoviesSuccess)
        whenever(viewModel.searchMoviesSuccess).thenReturn(searchMoviesSuccess)
        whenever(viewModel.getMovieSuccess).thenReturn(getMovieSuccess)
    }

    @Test
    fun shouldGetFavoriteMoviesWhenLaunched() {
        // when
        launchActivity()

        // then
        verify(viewModel, times(1)).getFavoriteMovies()
    }

    @Test
    fun shouldShowFavoriteMovies() {
        // given
        val movieOne = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            title = "Two"
            year = "2022"
            isFavorite = true
        }
        launchActivity()

        // when
        runOnUiThread { getFavoriteMoviesSuccess.postValue(listOf(movieOne, movieTwo)) }

        // then
        onView(withText(movieOne.title)).check(matches(isDisplayed()))
        onView(withText(movieOne.year)).check(matches(isDisplayed()))
        onView(withText(movieTwo.title)).check(matches(isDisplayed()))
        onView(withText(movieTwo.year)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldRemoveMovieFromFavorites() {
        // given
        val movie = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        launchActivity()
        runOnUiThread { getFavoriteMoviesSuccess.postValue(listOf(movie)) }

        // when
        onView(withId(R.id.favorite_button)).perform(click())

        // then
        verify(viewModel, times(1)).toggleFavoriteMovie(movie)
    }

    @Test
    fun shouldOpenFavoriteMovieDetails() {
        // given
        val movie = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        launchActivity()
        runOnUiThread { getFavoriteMoviesSuccess.postValue(listOf(movie)) }

        // when
        onView(withText(movie.title)).perform(click())

        // then
        intended(
            allOf(
                hasComponent(MovieActivity::class.java.name),
                hasExtra("movieId", movie.id)
            )
        )
    }

    @Test
    fun shouldOpenSearchScreen() {
        // given
        launchActivity()

        // when
        onView(withId(R.id.search_bar)).perform(click())

        // then
        intended(hasComponent(SearchActivity::class.java.name))
    }

    @Test
    fun shouldDisplayErrorMessage() {
        // given
        val errorMessage = "error"
        launchActivity()

        // when
        runOnUiThread { anyUseCaseFailure.postValue(Throwable(errorMessage)) }

        // then
        TestUtils.assertSnackbarMessageShown(errorMessage)
    }

    @Test
    fun shouldDisplayProgressDialog() {
        // given
        launchActivity()

        // when
        runOnUiThread { anyUseCaseInProgress.postValue(true) }

        // then
        waitForUiThread()
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()))
    }
}