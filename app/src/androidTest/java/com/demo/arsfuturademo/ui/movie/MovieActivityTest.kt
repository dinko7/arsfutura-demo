package com.demo.arsfuturademo.ui.movie

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.common.TestUtils
import com.demo.arsfuturademo.ui.base.BaseActivityTest
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@HiltAndroidTest
class MovieActivityTest: BaseActivityTest() {

    override val activityClass = MovieActivity::class.java

    @Mock
    @BindValue
    lateinit var viewModel: MovieViewModel

    private val getMovieSuccess = MutableLiveData<Movie>()

    @Before
    fun setUp() {
        whenever(viewModel.anyUseCaseFailure).thenReturn(anyUseCaseFailure)
        whenever(viewModel.getMovieSuccess).thenReturn(getMovieSuccess)
    }

    @Test
    fun shouldGetMovieWhenLaunchedIfIdWasProvided() {
        // given
        val movieId = "id"

        // when
        launchActivity(Bundle().apply {
            putString("movieId", movieId)
        })

        // then
        verify(viewModel, times(1)).getMovie(movieId)
    }

    @Test
    fun shouldDisplayMovieDetails() {
        val movie = Movie().apply {
            id = "id"
            imdbRating = "8.0"
            title = "title"
            description = "description"
            releaseDate = "04 May, 2012"
            duration = "100 min"
            genres = listOf("sci-fi","action","drama")
            isFavorite = true
        }
        launchActivity()

        // when
        runOnUiThread { getMovieSuccess.postValue(movie) }

        // then
        onView(allOf(withText(movie.title), withId(R.id.title))).check(matches(isDisplayed()))
        onView(withText(movie.description)).check(matches(isDisplayed()))
        onView(withSubstring(movie.imdbRating)).check(matches(isDisplayed()))
        onView(withSubstring(movie.releaseDate)).check(matches(isDisplayed()))
        onView(withSubstring(movie.duration)).check(matches(isDisplayed()))
        movie.genres!!.forEach { onView(withText(it)).check(matches(isDisplayed())) }
        onView(withId(R.id.is_favorite)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldToggleFavorite() {
        val movie = Movie().apply {
            id = "id"
            isFavorite = true
        }
        launchActivity()
        runOnUiThread { getMovieSuccess.postValue(movie) }

        // when
        onView(withId(R.id.is_favorite)).perform(click())

        // then
        verify(viewModel, times(1)).toggleFavoriteMovie(movie)
        onView(withId(R.id.is_not_favorite)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldDisplayErrorMessage() {
        // given
        val errorMessage = "error"
        launchActivity()

        // when
        runOnUiThread { anyUseCaseFailure.postValue(Throwable(errorMessage)) }

        // then
        TestUtils.assertSnackbarMessageShown(errorMessage)
    }
}