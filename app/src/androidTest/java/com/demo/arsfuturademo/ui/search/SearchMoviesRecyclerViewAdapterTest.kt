package com.demo.arsfuturademo.ui.search

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.demo.arsfuturademo.ui.base.BaseRecyclerViewAdapterTest
import com.demo.arsfuturademo.ui.common.stickyheader.StickyHeaderDecoration
import com.demo.domain.Movie
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Test

@HiltAndroidTest
class SearchMoviesRecyclerViewAdapterTest : BaseRecyclerViewAdapterTest() {

    val adapter = SearchMoviesRecyclerViewAdapter()

    @Before
    fun setUp() {
        launchActivity()
        runOnUiThread {
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(StickyHeaderDecoration(adapter, false, false))
        }
    }

    @Test
    fun shouldDisplayMoviesWithHeaders() {
        //this test works, but it is unable to find header views in view hierarchy, therefore this cannot be tested

        // given
        val movieOne = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            title = "Two"
            year = "2022"
            isFavorite = true
        }

        // when
        runOnUiThread { adapter.appendAll(listOf(movieOne, movieTwo)) }

        // then
        onView(withText(movieOne.title)).check(matches(isDisplayed()))
        onView(withText(movieTwo.title)).check(matches(isDisplayed()))
    }
}