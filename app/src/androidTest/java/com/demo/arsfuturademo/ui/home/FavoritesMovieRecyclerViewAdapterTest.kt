package com.demo.arsfuturademo.ui.home

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.demo.arsfuturademo.ui.base.BaseRecyclerViewAdapterTest
import com.demo.domain.Movie
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Test

@HiltAndroidTest
class FavoritesMovieRecyclerViewAdapterTest: BaseRecyclerViewAdapterTest() {
    private val adapter = FavoriteMoviesRecyclerViewAdapter()

    @Before
    fun setUp() {
        launchActivity()
        runOnUiThread { recyclerView.adapter = adapter }
    }

    @Test
    fun shouldDisplayFavoriteMovies() {
        // given
        val movieOne = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            title = "Two"
            year = "2022"
            isFavorite = true
        }

        // when
        runOnUiThread { adapter.appendAll(listOf(movieOne, movieTwo)) }

        // then
        onView(withText(movieOne.title)).check(matches(isDisplayed()))
        onView(withText(movieOne.year)).check(matches(isDisplayed()))
        onView(withText(movieTwo.title)).check(matches(isDisplayed()))
        onView(withText(movieTwo.year)).check(matches(isDisplayed()))
    }
}