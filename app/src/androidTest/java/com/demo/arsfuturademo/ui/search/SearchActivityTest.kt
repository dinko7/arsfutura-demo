package com.demo.arsfuturademo.ui.search

import android.view.KeyEvent
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.matcher.ViewMatchers.*
import com.demo.arsfuturademo.R
import com.demo.arsfuturademo.common.TestUtils
import com.demo.arsfuturademo.ui.base.BaseActivityTest
import com.demo.arsfuturademo.ui.movie.MovieActivity
import com.demo.arsfuturademo.viewmodel.MovieViewModel
import com.demo.domain.Movie
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.atLeast
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@HiltAndroidTest
class SearchActivityTest : BaseActivityTest() {
    override val activityClass = SearchActivity::class.java

    @Mock
    @BindValue
    lateinit var viewModel: MovieViewModel

    private val searchMoviesSuccess = MutableLiveData<List<Movie>>()
    private val getMovieSuccess = MutableLiveData<Movie>()

    @Before
    fun setUp() {
        whenever(viewModel.anyUseCaseFailure).thenReturn(anyUseCaseFailure)
        whenever(viewModel.anyUseCaseInProgress).thenReturn(anyUseCaseInProgress)
        whenever(viewModel.searchMoviesSuccess).thenReturn(searchMoviesSuccess)
        whenever(viewModel.getMovieSuccess).thenReturn(getMovieSuccess)
    }

    @Test
    fun shouldShowSearchResults() {
        // given
        val movieOne = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            title = "Two"
            year = "2022"
            isFavorite = true
        }
        launchActivity()

        // when
        runOnUiThread { searchMoviesSuccess.postValue(listOf(movieOne, movieTwo)) }

        // then
        onView(withText(movieOne.title)).check(matches(isDisplayed()))
        onView(withText(movieTwo.title)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldToggleFavoriteMovie() {
        // given
        val movie = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        launchActivity()
        runOnUiThread { searchMoviesSuccess.postValue(listOf(movie)) }

        // when
        onView(withId(R.id.favorite_button)).perform(click())

        // then
        verify(viewModel, times(1)).toggleFavoriteMovie(movie)
    }

    @Test
    fun shouldSearchMovies() {
        //given
        val searchQuery = "query"
        launchActivity()

        // when
        onView(withId(R.id.search)).perform(
            click(),
            typeTextIntoFocusedView(searchQuery),
            pressKey(KeyEvent.KEYCODE_ENTER)
        )


        //then
        verify(viewModel, atLeast(1)).searchMovies(searchQuery)
    }

    @Test
    fun shouldOpenFavoriteMovieDetails() {
        // given
        val movie = Movie().apply {
            id = "1"
            title = "One"
            year = "2021"
            isFavorite = true
        }
        launchActivity()
        runOnUiThread { searchMoviesSuccess.postValue(listOf(movie)) }

        // when
        onView(withText(movie.title)).perform(click())

        // then
        intended(
            allOf(
                hasComponent(MovieActivity::class.java.name),
                hasExtra("movieId", movie.id)
            )
        )
    }

    @Test
    fun shouldDisplayErrorMessage() {
        // given
        val errorMessage = "error"
        launchActivity()

        // when
        runOnUiThread { anyUseCaseFailure.postValue(Throwable(errorMessage)) }

        // then
        TestUtils.assertSnackbarMessageShown(errorMessage)
    }

    @Test
    fun shouldDisplayProgressDialog() {
        // given
        launchActivity()

        // when
        runOnUiThread { anyUseCaseInProgress.postValue(true) }

        // then
        waitForUiThread()
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()))
    }
}