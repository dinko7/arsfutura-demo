package com.demo.domain

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Movie {
    @PrimaryKey
    @NonNull
    var id: String? = null

    var title: String? = null
    var imageUrl: String? = null
    var year: String? = null
    var releaseDate: String? = null
    var duration: String? = null
    var genres: List<String>? = null
    var description: String? = null
    var imdbRating: String? = null
    var isFavorite: Boolean? = null
}