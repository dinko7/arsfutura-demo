package com.demo.data.repository

import com.demo.data.api.endpoint.OmdbApi
import com.demo.data.dao.FavoriteMovieDao
import com.demo.data.mapper.MovieMapper
import com.demo.domain.Movie
import javax.inject.Inject

class MovieRepository @Inject constructor(private val omdbApi: OmdbApi, private val movieMapper: MovieMapper,private val favoriteMovieDao: FavoriteMovieDao) {
    suspend fun searchMovies(keyword: String): List<Movie>? {
        val searchResult = omdbApi.searchMovies(keyword)
        val favorites = getFavoriteMovies().map { it.id }.toHashSet()
        val mappedResult = movieMapper.mapSearchMoviesResponseToMoviesList(searchResult)
        mappedResult?.forEach { it.isFavorite = favorites.contains(it.id) }
        return mappedResult
    }

    suspend fun getMovie(id: String): Movie {
        val response = omdbApi.getMovieInfo(id)
        return movieMapper.mapMovieInfoResponseToMovie(response).apply {
            isFavorite = favoriteMovieDao.getMovie(id) != null
        }
    }

    suspend fun getFavoriteMovies(): List<Movie> = favoriteMovieDao.getAll()

    suspend fun addToFavorites(movie: Movie) = favoriteMovieDao.insertAll(listOf(
        movie.apply { isFavorite = true }
    ))

    suspend fun removeFromFavorites(movie: Movie) = favoriteMovieDao.delete(movie)
}