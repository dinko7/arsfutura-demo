package com.demo.data.api.response

class MovieInfoResponse {
    var Title: String? = null
    var Year: String? = null
    var imdbID: String? = null
    var Poster: String? = null
    var Released: String? = null
    var Runtime: String? = null
    var Genre: String? = null
    var Plot: String? = null
    var imdbRating: String? = null
}
