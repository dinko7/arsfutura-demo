package com.demo.data.api.endpoint

import com.demo.data.api.response.MovieInfoResponse
import com.demo.data.api.response.SearchMoviesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbApi {
    @GET(".")
    suspend fun searchMovies(@Query("s") keyword: String): SearchMoviesResponse

    @GET(".")
    suspend fun getMovieInfo(@Query("i") id: String): MovieInfoResponse
}