package com.demo.data.network

import com.demo.data.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class UrlInterceptor : Interceptor {
    companion object {
        private const val apiKeyQueryParameterName = "apikey"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var original = chain.request()
        val url = original.url.newBuilder()
            .addQueryParameter(apiKeyQueryParameterName, BuildConfig.API_KEY).build()
        original = original.newBuilder().url(url).build()
        return chain.proceed(original)
    }
}