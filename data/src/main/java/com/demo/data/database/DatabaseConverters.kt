package com.demo.data.database

import androidx.room.TypeConverter

class DatabaseConverters {
    companion object {
        private const val listDelimiter = ","
    }
    @TypeConverter
    fun fromListOfStrings(list: List<String>?): String? = list?.joinToString(listDelimiter)

    @TypeConverter
    fun fromStringToListOfStrings(string: String?): List<String>? = string?.split(listDelimiter)
}