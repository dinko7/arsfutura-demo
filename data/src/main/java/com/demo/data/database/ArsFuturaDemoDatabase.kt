package com.demo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.demo.data.dao.FavoriteMovieDao
import com.demo.domain.Movie

@Database(entities = [Movie::class], version = 1)
@TypeConverters(DatabaseConverters::class)
abstract class ArsFuturaDemoDatabase : RoomDatabase() {
    abstract fun favoriteMovieDao(): FavoriteMovieDao
}