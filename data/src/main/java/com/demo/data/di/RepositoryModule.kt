package com.demo.data.di

import com.demo.data.api.endpoint.OmdbApi
import com.demo.data.dao.FavoriteMovieDao
import com.demo.data.mapper.MovieMapper
import com.demo.data.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Provides
    @Singleton
    fun provideMovieRepository(
        omdbApi: OmdbApi,
        movieMapper: MovieMapper,
        favoriteMovieDao: FavoriteMovieDao
    ) = MovieRepository(omdbApi, movieMapper, favoriteMovieDao)
}