package com.demo.data.di

import android.app.Application
import androidx.room.Room
import com.demo.data.database.ArsFuturaDemoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    private val databaseName = "arsfuturademo-db"

    @Provides
    @Singleton
    fun provideAppDatabase(application: Application) =
        Room.databaseBuilder(application, ArsFuturaDemoDatabase::class.java, databaseName)
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideFavoriteMovieDao(database: ArsFuturaDemoDatabase) = database.favoriteMovieDao()
}