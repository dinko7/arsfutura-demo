package com.demo.data.dao

import androidx.room.*
import com.demo.domain.Movie

@Dao
interface FavoriteMovieDao {
    @Query("SELECT * FROM movie")
    suspend fun getAll(): List<Movie>

    @Query("SELECT * FROM movie WHERE id = :id")
    suspend fun getMovie(id: String): Movie?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(favorites: List<Movie>)

    @Delete
    suspend fun delete(movie: Movie)
}