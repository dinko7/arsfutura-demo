package com.demo.data.mapper

import com.demo.data.api.response.MovieInfoResponse
import com.demo.data.api.response.SearchMoviesResponse
import com.demo.domain.Movie

class MovieMapper {
    fun mapSearchMoviesResponseToMoviesList(response: SearchMoviesResponse) = response.Search?.map {
        mapMovieInfoResponseToMovie(it)
    }

    fun mapMovieInfoResponseToMovie(response: MovieInfoResponse) = Movie().apply {
        response.let {
            id = it.imdbID
            title = it.Title
            year = it.Year
            imageUrl = it.Poster
            releaseDate = it.Released
            duration = it.Runtime
            description = it.Plot
            genres = it.Genre?.split(", ")
            imdbRating = it.imdbRating
        }
    }
}