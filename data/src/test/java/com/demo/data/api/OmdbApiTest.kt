package com.demo.data.api

import com.demo.data.api.endpoint.OmdbApi
import com.demo.data.api.response.MovieInfoResponse
import com.demo.data.api.response.SearchMoviesResponse
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class OmdbApiTest: BaseApiTest() {

    private lateinit var omdbApi: OmdbApi

    @Before
    fun setUp() {
        omdbApi = testRetrofit.create(OmdbApi::class.java)
    }

    @Test
    fun shouldSearchMoviesByKeyword() = runBlocking {
        // given
        val keyword = "keyword"
        val expectedResponse = SearchMoviesResponse().apply {
            Search = listOf(
                MovieInfoResponse().apply {
                    imdbID = "id"
                    Title = "title"
                }
            )
        }
        enqueueResponse(expectedResponse)

        // when
        val actualResponse = omdbApi.searchMovies(keyword)

        // then
        assertTrue(takeRequest().path!!.contains("?s=${keyword}"))
        assertEquals(expectedResponse.Search!!.size, actualResponse.Search!!.size)
        assertEquals(expectedResponse.Search!![0].imdbID, actualResponse.Search!![0].imdbID)
        assertEquals(expectedResponse.Search!![0].Title, actualResponse.Search!![0].Title)
    }

    @Test
    fun shouldGetMovieById() = runBlocking {
        // given
        val id = "id"
        val expectedResponse = MovieInfoResponse().apply {
                    imdbID = "id"
                    Title = "title"
                }
        enqueueResponse(expectedResponse)

        // when
        val actualResponse = omdbApi.getMovieInfo(id)

        // then
        assertTrue(takeRequest().path!!.contains("?i=${id}"))
        assertEquals(expectedResponse.imdbID, actualResponse.imdbID)
        assertEquals(expectedResponse.Title, actualResponse.Title)
    }
}