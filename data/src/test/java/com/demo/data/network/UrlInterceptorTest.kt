package com.demo.data.network

import com.demo.data.BuildConfig
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test

class UrlInterceptorTest : BaseInterceptorTest() {

    private val interceptor = UrlInterceptor()

    @Before
    fun setup() {
        addInterceptor(interceptor)
    }

    @Test
    fun shouldAddApiKey() {
        // when
        fireTestRequestWithResponse(Any())

        // then
        val request = takeRequest()
        assertTrue(request.path!!.contains("?apikey=${BuildConfig.API_KEY}"))
    }
}