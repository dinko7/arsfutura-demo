package com.demo.data.network

import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.net.HttpURLConnection

abstract class BaseInterceptorTest {
    @get:Rule
    var rule: MockitoRule = MockitoJUnit.rule()

    private val mockWebServer = MockWebServer()

    private lateinit var okHttpClient: OkHttpClient

    @Before
    fun startServer() {
        mockWebServer.start()
    }

    @After
    fun shutdownServer() {
        mockWebServer.shutdown()
    }

    protected fun fireTestRequestWithResponse(body: Any, responseCode: Int = HttpURLConnection.HTTP_OK) {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(responseCode).setBody(Gson().toJson(body))
        )
        okHttpClient.newCall(Request.Builder().url(mockWebServer.url("/")).build()).execute()
    }

    protected fun addInterceptor(interceptor: Interceptor) {
        okHttpClient = OkHttpClient().newBuilder().addInterceptor(interceptor).build()
    }

    protected fun takeRequest() = mockWebServer.takeRequest()
}