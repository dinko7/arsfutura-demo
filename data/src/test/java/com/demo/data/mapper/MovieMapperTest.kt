package com.demo.data.mapper

import com.demo.data.api.response.MovieInfoResponse
import com.demo.data.api.response.SearchMoviesResponse
import com.demo.domain.Movie
import junit.framework.TestCase.assertEquals
import org.junit.Test

class MovieMapperTest {
    private val movieInfoResponse = MovieInfoResponse().apply {
        imdbID = "id"
        Title = "title"
        Plot = "plot"
        Poster = "url"
        Genre = "sci-fi, action"
        Year = "2012"
        Released = "04 May, 2012"
        Runtime = "100 min"
        imdbRating = "5.0"
    }

    @Test
    fun shouldMapSearchMoviesResponseToMoviesList() {
        val input = SearchMoviesResponse().apply {
            Search = listOf(movieInfoResponse)
        }

        // when
        val output = MovieMapper().mapSearchMoviesResponseToMoviesList(input)

        // then
        assertEquals(input.Search!!.size, output!!.size)
        assertEqualMovie(input.Search!![0], output[0])
    }

    @Test
    fun shouldMapMovieInfoResponseToMovie() {
        // given
        val input = movieInfoResponse

        // when
        val output = MovieMapper().mapMovieInfoResponseToMovie(input)

        // then
        assertEqualMovie(input, output)
    }

    private fun assertEqualMovie(inputMovie: MovieInfoResponse, outputMovie: Movie) {
        assertEquals(inputMovie.imdbID, outputMovie.id)
        assertEquals(inputMovie.Title, outputMovie.title)
        assertEquals(inputMovie.Plot, outputMovie.description)
        assertEquals(inputMovie.Poster, outputMovie.imageUrl)
        assertEquals(inputMovie.Year, outputMovie.year)
        assertEquals(inputMovie.Released, outputMovie.releaseDate)
        assertEquals(inputMovie.Runtime, outputMovie.duration)
        assertEquals(inputMovie.imdbRating, outputMovie.imdbRating)
        assertEqualGenres(inputMovie.Genre, outputMovie.genres)
    }

    private fun assertEqualGenres(inputGenres: String?, outputGenres: List<String>?) {
        val genres = outputGenres?.joinToString(", ")
        assertEquals(inputGenres, genres)
    }
}