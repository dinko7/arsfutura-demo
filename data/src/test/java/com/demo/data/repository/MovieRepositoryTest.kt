package com.demo.data.repository

import com.demo.data.api.endpoint.OmdbApi
import com.demo.data.api.response.MovieInfoResponse
import com.demo.data.api.response.SearchMoviesResponse
import com.demo.data.dao.FavoriteMovieDao
import com.demo.data.mapper.MovieMapper
import com.demo.domain.Movie
import junit.framework.TestCase.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class MovieRepositoryTest: BaseRepositoryTest() {
    @Mock
    private lateinit var omdbApi: OmdbApi

    @Mock
    private lateinit var movieMapper: MovieMapper

    @Mock
    private lateinit var favoriteMovieDao: FavoriteMovieDao

    @InjectMocks
    private lateinit var movieRepository: MovieRepository

    @Test
    fun shouldSearchMovies() = runBlocking {
        // given
        val keyword = "keyword"
        val apiResponse = SearchMoviesResponse().apply {
            Search = listOf(
                MovieInfoResponse().apply { imdbID = "1" },
                MovieInfoResponse().apply { imdbID = "2" }
            )
        }
        val movieOne = Movie().apply { id = "1" }
        val movieTwo = Movie().apply { id = "2" }
        val mapperResult = listOf(movieOne, movieTwo)
        whenever(omdbApi.searchMovies(keyword)).thenReturn(apiResponse)
        whenever(movieMapper.mapSearchMoviesResponseToMoviesList(apiResponse)).thenReturn(mapperResult)
        whenever(favoriteMovieDao.getAll()).thenReturn(listOf(movieOne))

        // when
        val searchResult = movieRepository.searchMovies(keyword)

        // then
        verify(omdbApi, times(1)).searchMovies(keyword)
        verify(movieMapper, times(1)).mapSearchMoviesResponseToMoviesList(apiResponse)
        verify(favoriteMovieDao, times(1)).getAll()
        assertEquals(searchResult!!.size, mapperResult.size)
        assertEquals(searchResult[0].id, mapperResult[0].id)
        assertTrue(searchResult[0].isFavorite!!)
        assertEquals(searchResult[1].id, mapperResult[1].id)
    }

    @Test
    fun shouldGetMovie() = runBlocking {
        // given
        val movieId = "1"
        val apiResponse = MovieInfoResponse().apply { imdbID = movieId }
        val mapperResult = Movie().apply { id = movieId }
        whenever(omdbApi.getMovieInfo(movieId)).thenReturn(apiResponse)
        whenever(movieMapper.mapMovieInfoResponseToMovie(apiResponse)).thenReturn(mapperResult)

        // when
        val movie = movieRepository.getMovie(movieId)

        // then
        assertEquals(movie.id, movieId)
        assertFalse(movie.isFavorite!!)
    }

    @Test
    fun shouldGetFavoriteMovies() = runBlocking {
        // given
        val movieOne = Movie().apply {
            id = "1"
            isFavorite = true
        }
        val movieTwo = Movie().apply {
            id = "2"
            isFavorite = true
        }
        val daoResponse = listOf(movieOne,movieTwo)
        whenever(favoriteMovieDao.getAll()).thenReturn(daoResponse)

        // when
        val favoriteMovies = movieRepository.getFavoriteMovies()

        // then
        assertEquals(favoriteMovies.size, daoResponse.size)
        assertEquals(favoriteMovies[0].id, daoResponse[0].id)
        assertTrue(favoriteMovies[0].isFavorite!!)
        assertEquals(favoriteMovies[1].id, daoResponse[1].id)
        assertTrue(favoriteMovies[1].isFavorite!!)
    }

    @Test
    fun shouldAddMovieToFavorites(): Unit = runBlocking {
        // given
        val movie = Movie().apply { id = "1" }

        // when
        movieRepository.addToFavorites(movie)

        // then
        argumentCaptor<List<Movie>>().apply {
            verify(favoriteMovieDao, times(1)).insertAll(capture())
            assertEquals(firstValue[0].id, movie.id)
            assertTrue(firstValue[0].isFavorite!!)
        }
    }

    @Test
    fun shouldRemoveMovieFromFavorites() = runBlocking {
        // given
        val movie = Movie().apply { id = "1" }

        // when
        movieRepository.removeFromFavorites(movie)

        // then
        verify(favoriteMovieDao, times(1)).delete(movie)
    }
}