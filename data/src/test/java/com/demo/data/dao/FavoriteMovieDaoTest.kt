package com.demo.data.dao

import com.demo.domain.Movie
import junit.framework.TestCase
import junit.framework.TestCase.*
import kotlinx.coroutines.runBlocking
import org.junit.Test

class FavoriteMovieDaoTest: BaseDaoTest() {

    @Test
    fun shouldInsertFavoriteMovies() = runBlocking {
        // given
        val movieOne = Movie().apply { id = "1" }
        val movieTwo = Movie().apply { id = "2" }

        // when
        testDatabase.favoriteMovieDao().insertAll(listOf(movieOne,movieTwo))

        // then
        val moviesFromDb = testDatabase.favoriteMovieDao().getAll()
        assertNotNull(moviesFromDb.find { it.id == movieOne.id })
        assertNotNull(moviesFromDb.find { it.id == movieTwo.id })
    }

    @Test
    fun shouldDeleteFavoriteMovies() = runBlocking {
        // given
        val movie = Movie().apply { id = "1" }
        testDatabase.favoriteMovieDao().insertAll(listOf(movie))

        // when
        testDatabase.favoriteMovieDao().delete(movie)

        // then
        val favoritesFromDb = testDatabase.favoriteMovieDao().getAll()
        assertNull(favoritesFromDb.find { it.id == movie.id })
    }

    @Test
    fun shouldGetMovie() = runBlocking {
        // given
        val movieOne = Movie().apply { id = "1" }
        val movieTwo = Movie().apply { id = "2" }
        testDatabase.favoriteMovieDao().insertAll(listOf(movieOne, movieTwo))

        // when
        val movie = testDatabase.favoriteMovieDao().getMovie(movieTwo.id!!)

        // then
        assertEquals(movie?.id, movieTwo.id)
    }
}